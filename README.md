# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Introduction ###

MyWishList vous permet de créer vos listes d'envies, participer à celle des autres et bien plus !
C'est le site indispensable pour les cadeaux !

### Instructions d'installation ###

- Créer une base de données 'mywishlist'
- Mettre en place la base de données grâce au fichier creation_table.sql qui se trouve dans le dossier install du dépôt git
- Télecharger le fichier conf.ini qui se trouve dans le dossier install et le completer avec vos informations. Placer le ensuite dans src/conf/
- Autoriser les droits d'écriture au dossier img à la racine du projet
- Configurer le composer.json pour votre version de php (defaut:php 5.6)
- Ouvrir un terminal à la racine du projet et faire un composer install


### Développeurs du site ###

Luc Cheng, Thomas Serres et Antoine Delaroue
