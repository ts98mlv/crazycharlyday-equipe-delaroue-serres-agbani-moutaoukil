<?php
require_once 'vendor/autoload.php';

use garage\controleurs\ControleurPrincipal;
use garage\controleurs\ControleurItem;
use garage\controleurs\ControleurAdministration;
use garage\controleurs\Authentification;
use Illuminate\Database\Capsule\Manager as DB;
use Slim\Slim as Slim;

$app = new Slim();
$config = parse_ini_file('conf/conf.ini');
$DB = new DB();
$DB->addConnection($config);
$DB->setAsGlobal();
$DB->bootEloquent();

// On demarre la variable de session
if (! isset($_SESSION['user']))
    session_start();

$app->get("/connexion-inscription", function () {
    $c = new ControleurPrincipal();
    $c->afficherConnexion();
})->name("connexion-inscription");

$app->get("/", function () {
    $c = new ControleurPrincipal();
    $c->afficherAccueil();
})->name('accueil');

$app->post("/connexion", function () {
    $c = new Authentification();
    $c->authenticate();
})->name("connexion");

$app->post("/inscription", function () {
    $c = new Authentification();
    $c->createUser();
})->name("inscription");

$app->get("/logout", function () {
    $c = new Authentification();
    $c->logout();
})->name("logout");

$app->post("/creer-item", function () {
    $c = new ControleurItem();
    $c->creerItem();
})->name("creer-item");

$app->get("/creation-item", function () {
    $c = new ControleurItem();
    $c->afficherCreationItem();
})->name("creation-item");

$app->post("/modifier-item/:id_item", function ($id_item) {
    $c = new ControleurItem();
    $c->modifierItem($id_item);
})->name("modifier-item");

$app->get("/modification-item/:id_item", function ($id_item) {
    $c = new ControleurItem();
    $c->afficherModificationItem($id_item);
})->name("modification-item");

$app->get("/supprimer-item/:id_item", function ($id_item) {
    $c = new ControleurItem();
    $c->supprimerItem($id_item);
})->name("supprimer-item");

$app->get("/reservation-item/:id_item", function ($id_item) {
    $c = new ControleurItem();
    $c->afficherCreationReservation($id_item);
})->name("reservation-item");


$app->get("/ajout-commentaire/:id_item", function ($id_item) {
    $c = new ControleurItem();
    $c->ajoutCommentaire($id_item);
})->name("ajout-commentaire");

$app->get("/reserver-item/:id_item/:nb_jour/:nb_creneau", function ($id_item, $nb_jour, $nb_creneau) {
    $c = new ControleurItem();
    $c->reserverItem($id_item, $nb_jour, $nb_creneau);
})->name("reserver-item");

$app->get("/mes-participations", function () {
    $c = new ControleurPrincipal();
    $c->afficherParticipations();
})->name("mes-participations");

$app->get("/parametres", function () {
    $c = new ControleurPrincipal();
    $c->afficherInfosPersos_Param();
})->name("parametres");

$app->post("/parametres/modifier", function () {
    $c = new Authentification();
    $c->modifierParametres();
})->name("modifier-parametres");

$app->get("/suppression-compte", function () {
    $c = new ControleurPrincipal();
    $c->confirmerSuppressionCompte();
})->name("suppression-compte");

$app->get("/supprimer-compte", function () {
    $c = new Authentification();
    $c->supprimerCompte();
})->name("supprimer-compte");

$app->get("/reservations", function () {
    $c = new ControleurPrincipal();
    $c->afficherReservations();
})->name("reservation");

$app->get("/catalogue", function () {
    $c = new ControleurPrincipal();
    $c->afficherChoixCategorie();
})->name("catalogue");

$app->get("/voitures", function () {
    $c = new ControleurItem();
    $c->afficherVoitures();
})->name("voitures");

$app->get("/ateliers", function () {
    $c = new ControleurItem();
    $c->afficherAteliers();
})->name("ateliers");

$app->get("/administrer", function(){
  $c = new ControleurAdministration();
  $c->afficherPageAdmin();
})->name("administrer");

$app->get("/administrer/creer-compte", function(){
  $c = new ControleurAdministration();
  $c->afficherCreerCompte();
})->name("administrer/creer-compte");

$app->post("/administrer/creation-compte", function(){
  $c = new ControleurAdministration();
  $c->creerCompte();
})->name("administrer/creation-compte");

$app->get("/administrer/voir-comptes", function(){
  $c = new ControleurAdministration();
  $c->voirComptes();
})->name("administrer/voir-comptes");

$app->get("/administrer/modifier-compte/:id", function($id){
  $c = new ControleurAdministration();
  $c->afficherModifierCompte($id);
})->name("afficher-modifier-compte");

$app->post("/administrer/modification-compte/:id/", function($id){
  $c = new ControleurAdministration();
  $c->modifierCompte($id);
})->name("modification-compte");

$app->get("/administrer/reservations", function(){
  $c = new ControleurAdministration();
  $c->afficherReservations();
})->name("listes-reservees");

$app->run();
