-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30)  NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id_categorie`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `item` (
  `id_item` int(11) NOT NULL AUTO_INCREMENT ,
  `nom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `img` text,
  PRIMARY KEY (`id_item`),
  FOREIGN KEY (`id_categorie`) references `categorie`(`id_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `utilisateur` (
`id_user` int(11) NOT NULL AUTO_INCREMENT ,
`email` varchar(60) UNIQUE NOT NULL,
`prenom` varchar(30) not null,
`nom` varchar(30) not null,
`level` int(2) not null,
`mdp` varchar(256) not null,
PRIMARY KEY (`id_user`))ENGINE=InnoDB  CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `planning`(
  `id_planning` int(11) NOT NULL AUTO_INCREMENT ,
  `nom` text NOT NULL,
  `id_item` int(11),
  `id_user` int(11),
  FOREIGN KEY (`id_item`) references item(`id_item`),
  FOREIGN KEY (`id_user`) references utilisateur(`id_user`),
  PRIMARY KEY (`id_planning`)
)ENGINE=InnoDB AUTO_INCREMENT=1  CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `reservation` (
  `id_reservation` int(11) NOT NULL AUTO_INCREMENT ,
  `id_item` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nb_creneau` int(11) NOT NULL,
  `nb_jour` int(11) NOT NULL,
  `etat` varchar(10) NOT NULL,
  PRIMARY KEY (`id_reservation`),
  FOREIGN KEY (`id_item`) references item(`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=1  CHARSET=latin1;

INSERT INTO `categorie` (`id_categorie`, `nom`, `description`) VALUES
(1, 'Voitures', 'Une descritpion'),
(2, 'Ateliers', 'Une descritpion pour les ateliers');

INSERT INTO `item` (`nom`, `description`, `id_categorie`) VALUES
('Atelier en bois', 'Cet atelier en bois est l\'idéal pour réparer votre voiture tout en respirant la belle essence de Cyprès.', 2),
('Atelier BX023 en brique', 'Rustique, simple et fonctionnel, ce box vous permet de réparer votre véhicule sans vous perturber par son décorum. Un must pour les travaux difficiles !', 2),
('Batcave', 'L\'atelier qu\'il vous faut pour réparer secrètement votre batmobile (fourni sans Albert ni Bruce Wayne). ', 2),
('Atelier BX045', 'Sans lumière mais disposant d\'ouvertures au plafond, ce box est à réserver aux opérations les plus simples. Une lampe torche est fournie à l\'entrée pour que vous puissiez retrouver les pièces perdues.', 2),
('Atelier du futur', 'Avec cet atelier, vous serez déjà en l\'an 3000 !! Grand, bien agencé, ce box accueillera toutes vos voitures cylindriques dernier modèle.', 2),
('Atelier Miroir', 'Le fond de l\'atelier est tellement reflechissant qu\'on peut se voir dedans. Mr Propre y vient régulièrement. ', 2),
('Atelier du soleil', 'L\'atelier avec la plus belle vue pour pouvoir prendre de splendides photos et immortaliser ses réparations.', 2),
('Bentley', 'Bentley continentale, couleur gris métalisé, essence, deux portes. Ben t\'létait pas au courant ?', 1),
('Rolls Royce', 'Rolls Royce oldtimer, 12 places, voiture de 1978, restaurée. Sortez en famille en rolls Royce pour les plus grandes occasions.', 1),
('Opel', 'Envie de vous déplacer en toute discrétion dans les années 80, cette Opel est faite pour vous.', 1),
('Atelier securité', 'Pour effectuer vos réparations sans jamais être importuné, cet atelier propose de multiples volets métalliques insonorisés (ne limitent le propagation du son que fermés).', 2),
('Atelier multiple', 'Cet atelier permet d\'effectuer plusieurs réparations en simultanée. Un must pour les grands bricoleurs.', 2),
('Porshe 911', 'Porshe 911, noire, deux portes. Elegante et distinguée, la Rolls des voitures (juste aprés Rolls). Elle est tellement BELLE que l\'on écrit en majusCULES.', 1),
('Fiat 500', 'Fiat 500, Rouge avec son trait central blanc, diesel, deux portes. Petite mais costaude.', 1),
('Rolls Royce Cabriolet', 'Rolls Royce Cabriolet, jaune canari, 2 portes. Tentez votre chance, remportez tous les prix des courses d\'il y a 50 ans avec cette voiture.', 1),
('BMW 600', 'BMW 600, année 1957-1959, couleur bleu, une porte. Une seule porte mais tellement de place ! Ce serait dommage de ne pas la tester.', 1),
('R4 Renault', 'Renault R4, couleur rouge, 4 portes. Une voiture et un modèle qui n\'a pas vieilli.', 1),
('Batmobile (réplique)', 'Batmobile (réplique), couleur noire à bordereau rouge. Idéale pour aller chasser le Joker ou faire un coucou au Pinguoin (Robin non inclus). ', 1),
('Ferrari rouge', 'La Ferrari, la classique, la connue, la reputée, la pizza regina des voitures. What else ?', 1),
('Bus VW', 'Bus volkswagen, couleur vert-olive-pas-tout-a-fait-mure en bas, blanc en haut. Partir en famille sans se préoccuper de l\'espace disponible, c\'est possible !!', 1),
('Charette', 'Charette à bras, couleur bois, pratique et efficace, à locomotion forcée. A noter que les bras ne sont pas fournis avec le véhicule.', 1),
('Batmobile (la vraie)', 'N\'exigez qu\'une batmobile, la seule et l\'unique !!! Batmobile véritable construite dans les batiments de Wayne industrie. (ps: par contre, c\'est vrai que les répliques sont bien faites)', 1);
