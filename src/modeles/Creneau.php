<?php

namespace garage\modeles;
/*
* classe qui créer la reservation et la correspondance avec la base de donnée
*/
class Creneau extends \Illuminate\Database\Eloquent\Model{
    protected $table='creneau';
    protected $primaryKey='id_creneau';
    public $timestamps=false;
}
