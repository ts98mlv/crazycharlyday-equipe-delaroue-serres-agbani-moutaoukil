<?php

namespace garage\modeles;
/*
* classe qui créer un item et créer une correspondance avec la bdd
*/
class Item extends \Illuminate\Database\Eloquent\Model{
    protected $table='item';
    protected $primaryKey='id_item';
    public $timestamps=false;
}
