<?php

namespace garage\modeles;
/*
* classe qui créer la reservation et la correspondance avec la base de donnée
*/
class Reservation extends \Illuminate\Database\Eloquent\Model{
    protected $table='reservation';
    protected $primaryKey='id_reservation';
    public $timestamps=false;
}
