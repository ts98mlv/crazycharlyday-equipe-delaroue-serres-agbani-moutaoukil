<?php

namespace garage\modeles;
/*
* classe qui créer la reservation et la correspondance avec la base de donnée
*/
class Categorie extends \Illuminate\Database\Eloquent\Model{
    protected $table='categorie';
    protected $primaryKey='id_categorie';
    public $timestamps=false;
}
