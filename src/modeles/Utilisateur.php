<?php

namespace garage\modeles;

/*
* création de l'objet utilisateur et de la correspondance avec la base de donnéeS
*/
class Utilisateur extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'utilisateur';
    protected $primaryKey = 'id_user';
    public $timestamps = false;
}
