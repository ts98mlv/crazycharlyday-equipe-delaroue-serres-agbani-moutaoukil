<?php

namespace garage\vues;

use garage\modeles\Reservation;
use Slim\Slim;
use garage\modeles\Liste;
use garage\controleurs\ControleurListe;
use garage\modeles\Utilisateur;
use garage\modeles\Item;

/**
* Classe qui permet d'afficher les principaux élément du site
*/
class VuePrincipale{

  private $type, $parametres;
  const ACCUEIL=1;
  const CONNEXION_INSCRIPTION=2;
  const PARAMETRES=3;
  const SUPPRESSION_COMPTE=4;
  const ERREUR=5;
  const INFO=6;
  const INPUT=7;
  const CHOIX_CATEGORIE = 8;
  const UTILISATEUR = 9;
  const AIDE = 10;
  const ADMINISTRATEUR = 11;

  /***
  * Construit une VuePrincipale
  *   @param $t, int option pour la vue
  *   @param $par, mixed deuxieme parametre généralement egale a la liste
  *   @param $par2, mixed troisieme parametre généralement égal au items
  */
  public function __construct($t, $par = null){
    $this->type=$t;
    $this->parametres=$par;
  }

  /***
  * Affiche la page html que le navigateur va interpreter
  */
  public function render(){
    $head=$this->afficherHead();
    $header=$this->afficherHeader();
    $pied=$this->afficherPiedPage();
    $root=Slim::getInstance()->request->getRootUri();
    $rootAccueil = Slim::getInstance()->urlFor("accueil");
    //Si l'utilisateur est connécté on affiche la barre de navigation
    if(isset($_SESSION['user']))$nav=$this->afficherNav();
    else $nav='';

    switch($this->type){
      case VuePrincipale::ACCUEIL:
        $principal=$this->afficherAccueil();
        break;
      case VuePrincipale::CONNEXION_INSCRIPTION:
        $principal=$this->afficherConnexion().$this->afficherInscription();
        break;
      case VuePrincipale::ERREUR:
        $titre="ERREUR";
        $message=$this->parametres['message'];
        if(isset($parametres['lien']))$lien="$root/".$this->parametres['lien'];
        else $lien="$rootAccueil";
        $principal=$this->pageInformation($titre, $message, $lien);
        break;
      case VuePrincipale::INFO:
        $titre="INFORMATION";
        $message=$this->parametres['message'];
        if(isset($parametres['lien']))$lien="$root/".$this->parametres['lien'];
        else $lien="$rootAccueil";
        $principal=$this->pageInformation($titre, $message, $lien);
        break;
      case VuePrincipale::PARAMETRES:
        $principal=$this->pageParametres();
        break;
      case VuePrincipale::SUPPRESSION_COMPTE:
        $titre="ÊTES-VOUS SUR ?";
        $message="Êtes-vous sur de vouloir supprimer votre compte ? Toutes vos listes seront effacées.";
        $lien="$root/supprimer-compte";
        $principal=$this->pageInformation($titre, $message, $lien);
        break;
      case VuePrincipale::INPUT:
        $principal = $this->parametres['principal'];
        break;
      case VuePrincipale::CHOIX_CATEGORIE:
        $principal = $this->afficherChoixCategorie();
        break;
    }

    echo <<<END
  <!DOCTYPE html>
    <html lang="fr">
      $head
      <body>
        $header
        $nav
        $principal
        $pied
      </body>
    </html>
END;
  }

  /**
  * Génère le head, debut de la page html
  *   @return String correspondant à la balise head
  */
  public function afficherHead(){
    $root=Slim::getInstance()->request->getRootUri();
    return <<<END
    <head>
      <title>garage</title>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="stylesheet" href="$root/style.css" />
      <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
END;
  }

  /**
  * Génère le header, entete de la page
  *   @return String correspondant au à la balise header
  */
  public function afficherHeader(){
    $root=Slim::getInstance()->request->getRootUri();
    $rootAccueil=Slim::getInstance()->urlFor('accueil');
    $rootConnexionInscription=Slim::getInstance()->urlFor('connexion-inscription');
    $rootLogout=Slim::getInstance()->urlFor('logout');
    if(isset($_SESSION['user']))$connexion_state="<p><a href='$rootLogout'>Connecté: ".$_SESSION['user']['prenom']." ".$_SESSION['user']['nom']."<br /><br /><img src='$root/img/logout.png' height='60' width='60'></a></p>";
    else $connexion_state="<p><a href='$rootConnexionInscription'>Connexion/inscription <br /><br /><img src='$root/img/login.png' height='50' width='80'></a></p>";

    return <<<END
    <header>
      $connexion_state
      <a href='$rootAccueil'><h1>Pimp My Charlemagne</h1></a>
    </header>
END;
  }

  /**
  * Génère le nav, barre de navigation qui s'affiche seulement si on est connecté
  *   @return String correspondant au à la balise nav
  */
  public function afficherNav(){
    $root=Slim::getInstance()->request->getRootUri();
    $user = $_SESSION['user'];

    if($user->level === 0){
      $return =
      <<<END
      <nav>
        <a href="$root/catalogue"><p>Catalogue</p></a><a href="$root/reservations"><p>Mes reservations</p></a><a href="$root/parametres"><p>Informations/parametres</p></a><a href="$root/administrer"><p>Admin</p></a>
      </nav>;
END;
    }
    else{
    $return =
<<<END
  <nav>
    <a href="$root/catalogue"><p>Catalogue</p></a><a href="$root/reservations"><p>Mes reservations</p></a><a href="$root/parametres"><p>Informations/parametres</p></a>
  </nav>
END;
}

  return $return;
  }

  /**
  * Génère le contenu de l'accueil
  *   @return String correspondant a code html pour l'accueil
  */
  public function afficherAccueil(){
    $root=Slim::getInstance()->request->getRootUri();
    return
<<<END
    <div class="moitie">
      <div class="div_creation" style='width:80%;'>
        <h2 style="text-align: center;">Bienvenue sur garage !</h2>
        <p>Ce site est développé par Antoine, Thomas, Ismail et Yassin. Il vous permet de faire des réservations d'ateliers et de voitures!!<br> C'est le site indispensable pour les cadeaux !<br />
        Vous pouvez accéder au dépôt git <a style="text-decoration: underline; color: darkblue;" target="_blank" href="https://bitbucket.org/ts98mlv/crazycharlyday-equipe-delaroue-serres-agbani-moutaoukil">en cliquant ici</a>.</p>
        <br />
        <h4>Instructions d'installation :</h4>
        <ol>
          <li>- Créer une base de données 'garage'</li>
          <li>- Mettre en place la base de données grâce au fichier bdd.sql dans le dossier install du dépôt git</li>
          <li>- Télecharger le fichier conf.ini dans le dossier install du depot git et le compléter avec vos informations. Placer le ensuite dans src/conf/</li>
          <li>- Autoriser les droits d'écriture au dossier img à la racine du projet</li>
          <li>- Configurer le composer.json pour votre version de php (defaut:php 5.6)</li>
          <li>- Ouvrir un terminal à la racine du projet et faire un composer install</li>
        </ol>
      </div>
    </div>
END;
  }

  /**
  * Génère le div pour l'inscription
  *   @return String correspondant a code html pour le div inscription
  */
  public function afficherInscription(){
    $root=Slim::getInstance()->request->getRootUri();
    return
<<<END
    <div class="div_creation">
      <h4 style="text-align: center;">Pas de compte ? Inscrivez-vous !</h4>
      <form id="connexion_form" method="POST" action="$root/inscription">
        <label for="prenom">Prenom</label><br />
        <input type="text" name="prenom" required autocomplete='off' placeholder="Prenom..."><br /><br />

        <label for="nom">Nom</label><br />
        <input type="text" name="nom" required autocomplete='off' placeholder="Nom..."><br /><br />

        <label for="email">Email</label><br />
        <input type="email" name="email" required placeholder="Email..."><br /><br />

        <label for="mdp">Mot de passe</label><br />
        <input type="password" name="mdp" required placeholder="Entre 6 et 25 caractère"><br /><br />

        <label for="mdp_confirm">Confirmation du mot de passe</label><br />
        <input type="password" name="mdp_confirm" required placeholder="Entre 6 et 25 caractère"><br /><br />

        <button style="margin" type=submit>Inscription</button>
      </form>
    </div>
END;
  }

  /**
  * Génère le contenu du div pour la connexion
  *   @return String correspondant a code html le div de la connexion
  */
  public function afficherConnexion(){
    $root=Slim::getInstance()->request->getRootUri();
    $rootConnexion = Slim::getInstance()->urlFor('connexion');
    return
<<<END
    <div class="div_creation" style="margin-right:15%;">
      <h4 style="text-align: center;">Connectez vous pour acceder à votre compte</h4>
      <form id="connexion_form" method="POST" action="$rootConnexion">
        <input class="input" type="email" name="email" autofocus required placeholder="Email...">
        <input class="input" type="password" name="mdp" required placeholder="Mot de passe...">
        <button type=submit>Connexion</button>
      </form>
    </div>
END;
  }

  /**
  * Génère le contenu du pied de page
  *   @return String correspondant a code html pour le footer
  */
  public function afficherPiedPage(){
    $root=Slim::getInstance()->request->getRootUri();
    return
<<<END
    <footer>
      <p style='float:left; display:inline-block;'>&nbsp;&nbsp Antoine Delaroue, Thomas Serres, Ismail AGBANI, Yassin MOUTAOUKIL</p>
    </footer>
END;
  }

  /**
  * Génère le contenu d'une page d'information
  *   @param $titre String titre de la page d'info
  *   @param $message String message d'information
  *   @param $lienRetour String lien pour le bouton retour
  *   @return String correspondant a code html pour la page d'information
  */
  public function pageInformation($titre, $message, $lienRetour){
    return <<<END
      <div class="div_creation">
        <h4 style="text-align: center;">$titre</h4>
        <p>$message</p>
        <a class='button' style='float:none; margin-left: 45%;' href='$lienRetour'><p>OK</p></a>
      </div>
END;
  }

  /**
  * Génère le contenu pour la page de parametre
  *   @return String code html pour le contenu de la page de parametre
  */
  public function pageParametres(){
      $root=Slim::getInstance()->request->getRootUri();
      $rootModifierParametres = Slim::getInstance()->urlFor('modifier-parametres');
      $rootSupprimerCompte = Slim::getInstance()->urlFor('suppression-compte');
      $nom=$_SESSION['user']['nom'];
      $email=$_SESSION['user']['email'];
      $prenom=$_SESSION['user']['prenom'];

      return <<<END
        <div  class="div_creation">
          <h2>Bienvenue sur la page des paramètres</h2>
          <h4> Vous pouvez ici choisir de modifier vos informations personnelles</h4>
          <form id="modifier-parametres" method="POST" action="$rootModifierParametres">

            <label for="prenom">Prénom : </label><br />
            <input type="text" name="prenom" value='$prenom' required autocomplete='off' placeholder="prénom..."><br /><br />

            <label for="nom">Nom : </label><br />
            <input type="text" name="nom" value='$nom' required autocomplete='off' placeholder="nom..."><br /><br />

            <label for="mail">Email: </label><br />
            <input type="email" name="email" value='$email' required autocomplete='off' placeholder="adresse email..."><br /><br />

            <label for="mdp">Mot de passe</label><br />
            <input type="password" name="mdp" placeholder="Entre 6 et 25 caractère"><br /><br />

            <label for="mdp_confirm">Confirmation du mot de passe</label><br />
            <input type="password" name="mdp_confirm" placeholder="Entre 6 et 25 caractère"><br /><br />

            <button type=submit>Enregistrer</button>
          </form>
          <a class='button' href='$rootSupprimerCompte'>Supprimer mon compte</a>
        </div>
END;

  }

      public function afficherChoixCategorie(){
        $root=Slim::getInstance()->request->getRootUri();
        $rootAteliers = Slim::getInstance()->urlFor("ateliers");
        $rootVoitures = Slim::getInstance()->urlFor("voitures");
        return <<<END
<div class='moitie'><a href="$rootVoitures"><div style="background-image:url($root/img/voiture.jpg);" class='categorie'><h1>Voitures</h1></div></a></div><div class='moitie'><a href="$rootAteliers"><div style="background-image: url($root/img/atelier.jpeg);" class='categorie'><h1>Atelier</h1></div></a></div>
END;
      }
}
