<?php

namespace garage\vues;

use garage\modeles\Reservation;
use garage\vues\VuePrincipale;
use garage\modeles\Item;
use garage\modeles\Utilisateur;
use Slim\Slim as Slim;



/**
* Classe qui permet d'afficher les éléments relatifs a l'administration
*/
class VueAdministrateur extends VuePrincipale{

  const AFFICHAGE = 1;
  const CREERCOMPTE = 2;
  const AFFICHERCOMPTES = 3;
  const MODIFIERCOMPTE = 4;
  const AFFICHERLISTESRESERVEES = 5;

  private $type, $parametres;

  /**
  * Construit une VueAdministrateur
  *   @param $t, int option pour la vue
  *   @param $par, mixed deuxieme parametre généralement egale a un item
  */
  public function __construct($t, $par = null){
    $this->type=$t;
    $this->parametres=$par;
  }

  /**
  * Affiche la page html que le navigateur va interpreter
  */
  public function render(){
    $root=Slim::getInstance()->request->getRootUri();
    switch($this->type){
      case VueAdministrateur::AFFICHAGE:
        $principal=$this->afficherPageAdmin();
        break;
      case VueAdministrateur::CREERCOMPTE:
        $principal=$this->afficherCreerCompte();
        break;
      case VueAdministrateur::AFFICHERCOMPTES:
        $principal = $this->voirComptes();
        break;
      case VueAdministrateur::MODIFIERCOMPTE:
        $principal = $this->afficherModifierCompte();
        break;
      case VueAdministrateur::AFFICHERLISTESRESERVEES:
        $principal = $this->afficherListesReservees();
        break;
    }

    $v = new VuePrincipale(VuePrincipale::INPUT, ['principal' => $principal]);
    $v->render();
  }

/**
*affiche la page d'administration
*/
public function afficherPageAdmin(){
  $root=Slim::getInstance()->request->getRootUri();

  $ligne = <<<END
    <div  class="div_creation">
      <h1>Bienvenue sur la page des paramètres</h1>
      <h2> Vous pouvez ici choisir de paramétrer le site</h2>

      <a class='button' href='$root/administrer/creer-compte'>créer un compte</a>
      <a class='button' href='$root/administrer/voir-comptes'>voir les comptes existants</a>
      <a class='button' onClick="alert('fonctionnalitée pas encore implémentée')">voir les réservations en attentes</a>
      <a class='button' href='$root/administrer/reservations'">voir les réservations confirmées</a>

    </div>
END;

  return $ligne;
}

public function afficherCreerCompte(){
$root=Slim::getInstance()->request->getRootUri();


return <<<END
  <div  class="div_creation">
    <h2>Bienvenue sur la page des paramètres</h2>
    <h4> Vous pouvez ici choisir de modifier vos informations personnelles</h4>
    <form id="modifier-parametres" method="POST" action="$root/administrer/creation-compte">

      <label for="prenom">Prénom : </label><br />
      <input type="text" name="prenom" required autocomplete='off' placeholder="prénom..."><br /><br />

      <label for="nom">Nom : </label><br />
      <input type="text" name="nom" required autocomplete='off' placeholder="nom..."><br /><br />

      <label for="mail">Email: </label><br />
      <input type="email" name="email" required autocomplete='off' placeholder="adresse email..."><br /><br />

      <label for="mdp">Mot de passe</label><br />
      <input type="password" name="mdp" placeholder="Entre 6 et 25 caractère"><br /><br />

      <label for="mdp_confirm">Confirmation du mot de passe</label><br />
      <input type="password" name="mdp_confirm" placeholder="Entre 6 et 25 caractère"><br /><br />

      <label for="level">Entrez le niveau du compte </label><br/>
      <select name="level" size="1">
      <option>
      compte standard
      </option>
      <option>
      compte administrateur
      </option>
      </select><br />

      <button type=submit class="button">Enregistrer</button>

  </div>
END;

}

/**
*methode qui permet a l'administrateur de voir tous les comptes existants
*/
public function voirComptes(){
    $root=Slim::getInstance()->request->getRootUri();
    $comptes=$this->parametres['comptes'];

    if($comptes==null || count($comptes)==0){
      $lignes="Il n'y a aucun compte";
    }
    else{
      $lignes='';
      foreach ($comptes as $compte) {
        $actions = "<a class='button' href='$root/administrer/modifier-compte/$compte->id_user'><p>Modifier</p></a>";

        if($compte->level === 0){
          $lvl = "compte administrateur";
        }
        else{
          $lvl = "compte standard";
        }

      $lignes.= <<<END
        <div class='div_ligne_liste'>
      <div class='div_contenu'>
          <p>
          Nom : $compte->nom<br />
          Prénom : $compte->prenom<br/>
          Email : $compte->email <br />
          Niveau : $lvl <br/>
          </div>
          <div class='div_action'>$actions</div>
          </p>

        </div>
END;
        if($comptes[sizeof($comptes)-1]!=$compte)$lignes.="<hr />";
      }
    }
    return <<<END
    <div id='div_entete'>
      <div class='div_contenu'><h1>Les comptes existants</h1></div>
    </div>
    <div id='div_liste'>
      $lignes
    </div>
END;
}

/**
*methode qui permet d'afficher la modification du compte
*/
public function afficherModifierCompte(){
$root=Slim::getInstance()->request->getRootUri();
$user = $this->parametres['compte'];
$nom = $user->nom;
$prenom = $user->prenom;
$email = $user->email;
$id=$this->parametres['id'];

if($user->level === 0)
  $combobox = <<<CB
  <label for="level">Entrez le niveau du compte </label><br/>
  <select name="level" size="1">
  <option>
  compte standard
  </option>
  <option selected="selected">
  compte administrateur
  </option>
  </select><br />
CB;
else {
    $combobox = <<<CB
    <label for="level">Entrez le niveau du compte </label><br/>
    <select name="level" size="1">
    <option selected="selected">
    compte standard
    </option>
    <option>
    compte administrateur
    </option>
    </select><br />
CB;
}


return <<<END
  <div  class="div_creation">
    <h2>Bienvenue sur la page des paramètres</h2>
    <h4> Vous pouvez ici choisir de modifier vos informations personnelles</h4>
    <form id="modifier-parametres" method="POST" action="$root/administrer/modification-compte/$id">

      <label for="prenom">Prénom : </label><br />
      <input type="text" name="prenom" required autocomplete='off' autofocus value="$prenom" placeholder="prénom..."><br /><br />

      <label for="nom">Nom : </label><br />
      <input type="text" name="nom" required autocomplete='off' value ="$nom" placeholder="nom..."><br /><br />

      <label for="mail">Email: </label><br />
      <input type="email" name="email" required autocomplete='off' value ="$email" placeholder="adresse email..."><br /><br />

    $combobox

      <button type=submit class="button">Enregistrer</button>

  </div>
END;

}

/**
*methode qui permet de voir les listes reservees
*/
public function afficherListesReservees(){
$listes = $this->parametres['listes'];

if($listes==null || count($listes)==0){
  $lignes="Il n'y a aucune réservation";
}
else{
  $lignes='';
  foreach ($listes as $liste) {

    switch($liste->nb_jour){
      case 1:
        $jour = "lundi";
        break;
      case 2:
        $jour = "mardi";
        break;
      case 3:
        $jour = "mercredi";
        break;
      case 4:
        $jour = "jeudi";
        break;
      case 5:
        $jour = "vendredi";
        break;
      case 6:
        $jour = "samedi";
        break;
      case 7:
        $jour = "dimanche";
        break;
    }

    switch($liste->nb_creneau){
      case 1:
        $creneau = "8h - 10h";
        break;
      case 2:
        $creneau = "10h - 12h";
        break;
      case 3:
        $creneau = "12h - 14h";
        break;
      case 4:
        $creneau = "14h - 16h";
        break;
      case 5:
        $creneau = "16h - 18h";
        break;

    }

  $lignes.= <<<END
    <div class='div_ligne_liste'>
  <div class='div_contenu'>
      <p>
      <b>Nom item réservé :</b> $liste->nom<br />
      <b>Description item réservé :</b> $liste->description<br/>
      <b>Jour de la semaine : </b>$jour <br />
    <b> Horaires créneau :</b> $creneau <br/>
      </div>
      </p>

    </div>
END;
    if($listes[sizeof($listes)-1]!=$liste)$listes.="<hr />";
  }
}
return <<<END
<div id='div_entete'>
  <div class='div_contenu'><h1>Les réservations</h1></div>
</div>
<div id='div_liste'>
  $lignes
</div>
END;

}
}
