<?php

namespace garage\vues;

use garage\modeles\Reservation;
use garage\vues\VuePrincipale;
use garage\modeles\Item;
use garage\modeles\Utilisateur;
use Slim\Slim as Slim;



/**
* Classe qui permet d'afficher les éléments relatifs au items
*/
class VueItem extends VuePrincipale{

  const CREATION=1;
  const MODIFICATION=2;
  const RESERVATION=3;
  const MESRESERVATIONS = 4;
  const AFFICHER=5;

  private $type, $parametres;

  /**
  * Construit une VueItem
  *   @param $t, int option pour la vue
  *   @param $par, mixed deuxieme parametre généralement egale a un item
  */
  public function __construct($t, $par = null){
    $this->type=$t;
    $this->parametres=$par;
  }

  /**
  * Affiche la page html que le navigateur va interpreter
  */
  public function render(){
    $root=Slim::getInstance()->request->getRootUri();
    switch($this->type){
      case VueItem::CREATION:
        $principal=$this->afficherCreationItem();
        break;
      case VueItem::MODIFICATION:
        $principal = $this->afficherModificationItem();
        break;
      case VueItem::RESERVATION:
        $principal = $this->afficherReservationItem();
        break;
      case VueItem::AFFICHER:
        $principal = $this->afficherItems();
        break;
      case VueItem::MESRESERVATIONS:
        $principal = $this->afficherMesReservations();
        break;
    }

    $v = new VuePrincipale(VuePrincipale::INPUT, ['principal' => $principal]);
    $v->render();
  }

  /**
  * Génère l'affichage d'un item
  *   @param Item item à afficher
  *   @return String ensemble de balises correspondant à l'affichage d'un item
  */
  public static function afficherItem($item){
    $root=Slim::getInstance()->request->getRootUri();
    return <<<END
        <img class='img_item' src='$root/img/$item->img' title='$root/img/$item->img' alt='$item->img' height='80' width='80'>
        <h4>$item->nom</h4>
        <p>
          <i>$item->description</i><br />
        </p>
END;
  }

  /**
  * Génère la page pour la création d'un item
  *   @return String ensemble de balises correspondant à l'affichage de la page de creation d'item
  */
  public function afficherCreationItem(){
    $root=Slim::getInstance()->request->getRootUri();
    $rootCreerItem = Slim::getInstance()->urlFor('creer-item');
    return <<<END
    <div>
      <h4>Créer un item</h4>
      <form id="creer-liste" method="POST" action="$rootCreerItem" enctype="multipart/form-data">
        <label for="nom">Nom</label><br />
        <input type="text" name="nom" required autofocus autocomplete='off' placeholder="Nom de l'item..."><br /><br />

        <label for="descr">Description</label><br />
        <textarea required placeholder="Description..." name="descr" rows="5" cols="40"></textarea><br /><br />

        <label for="image">Image (tous formats | max. 10 Mo) :</label><br />
        <input type="file" name="image"/><br /><br /><br />

        <label for="descr">Description</label><br />
        <textarea required placeholder="Description..." name="descr" rows="5" cols="40"></textarea><br /><br />

        <button type=submit>Enregistrer</button>
      </form>
    </div>
END;
  }

  /**
  * Génère la page pour la modification d'un item
  *   @return String ensemble de balises correspondant à l'affichage de la page de creation d'item
  */
  public function afficherModificationItem(){
    $root=Slim::getInstance()->request->getRootUri();
    $nom=$this->parametres['item']->nom;
    $description=$this->parametres['item']->descr;
    $tarif=$this->parametres['item']->tarif;
    $img=$this->parametres['item']->img;
    $rootModifierItem=Slim::getInstance()->urlFor('modifier-item', ['id_item' => $this->parametres['item']->id_item]);

    return <<<END
    <div class="div_creation">
      <h4 style="text-align: center;">Modifier un item</h4>
      <form id="creer-liste" method="POST" action="$rootModifierItem" enctype="multipart/form-data">
        <label for="nom">Nom</label><br />
        <input type="text" name="nom" required autofocus value='$nom' autocomplete='off' placeholder="Nom de l'item..."><br /><br />

        <label for="descr">Description</label><br />
        <textarea required placeholder="Description..." name="descr" rows="5" cols="40">$description</textarea><br /><br />

        <label for="tarif">Prix</label><br />
        <input type="number" value='$tarif' name="tarif" required min="0" step="0.01"> €<br /><br />

        <label for="image">Image (tous formats | max. 10 Mo) :</label><br />
        <input type="file" name="image"/><br /><br /><br />

        <input type="radio" name="gender" value="female">Female
        <input type="radio" name="gender" value="male">Male

        <button type=submit>Enregistrer</button>
      </form>
    </div>
END;
  }


  /**
  * Génère la page pour la réservation d'un item
  *   @return String ensemble de balises correspondant à l'affichage de la page de réservation d'item
  */
  public function afficherReservationItem(){
    $item=$this->parametres['item'];
    $root=Slim::getInstance()->request->getRootUri();
    $div_contenu=$this->afficherItem($item);
    $reservations = $this->parametres['reservations'];
    $lignes="<div class='div_planning'>";
    for($i = 1; $i <= 7; $i++){
      $lignes.="<div class='div_jour'>";
      for($j = 1; $j <= 5; $j++){
        $reservation = $reservations->where('nb_jour', '=', $i)->where('nb_creneau', '=', $j)->first();
        if($reservation != NULL){
          $lignes.="<div class='div_creneau reserve'><p>Reservé</p></div>";
        }
        else{
          $rootReserverItem=Slim::getInstance()->urlFor('reserver-item', ['id_item' => $item->id_item, 'nb_jour' => $i, 'nb_creneau' => $j]);
          $lignes.="<div class='div_creneau libre'><a href='$rootReserverItem'>Reserver</a></div>";
        }
      }
      $lignes.="</div>";
    }
    $lignes.="</div>";

    return <<<END
    <div class="div_creation">
      <h2>Reserver un item</h2>
      $div_contenu
      <br /><br />
      <p>Veullez saisir les informations de reservation</p>
      <br>
      $lignes
    </div>
END;
  }
  /**
  *methode qui gère l'affichage des reservations de l'utilisateur
  */
  public function afficherMesReservations(){
    $root=Slim::getInstance()->request->getRootUri();
    $reservations = $this->parametres['reservations'];
    $lignes="<div class='div_planning'>";
    for($i = 1; $i <= 7; $i++){
      $lignes.="<div class='div_jour'>";
      for($j = 1; $j <= 5; $j++){
        $reservation = $reservations->where('nb_jour', '=', $i)->where('nb_creneau', '=', $j)->first();
        if($reservation != NULL){
          $item = Item::where('id_item', '=', $reservation->id_item)->first();
          $lignes.="<div class='div_creneau reserve'><p>$item->nom</p></div>";
        }
        else{
          $lignes.="<div class='div_creneau libre'><p>Libre</p></div>";
        }
      }
      $lignes.="</div>";
    }
    $lignes.="</div>";

    return <<<END
    <div class="div_creation">
      <h2>Votre planning</h2>
      <br /><br />
      <p>Vous avez reserve pour ces créneaux :</p>
      <br>
      $lignes
    </div>
END;
/*
    $root=Slim::getInstance()->request->getRootUri();
    $reservations=$this->parametres['reservations'];

    if($reservations==null || count($reservations)==0){
      $lignes="Vous n'avez aucune réservation";
    }
    else{
      $lignes='';
      foreach ($reservations as $reservation) {
      $item = Item::where('id_item', '=', $reservation->id_item)->first();
        $lignes.= <<<END
        <div class='div_ligne_liste'>
          <p>
          Réservation n°$reservation->id_reservation <br />
          Item réservé : $item->nom <br />
          Description de l'item : $item->description <br />
          </p>
        </div>
END;
      if($reservations[sizeof($reservations)-1]!=$reservation)$lignes.="<hr>";

      }
    }
    return <<<END
    <div id='div_entete'>
      <div class='div_contenu'><h1>Vos réservations</h1></div>
    </div>
    <div id='div_liste'>
      $lignes
    </div>
END;*/
}

public function afficherItems(){
  $items = $this->parametres['items'];
  $categorie = $this->parametres['categorie'];
  $root=Slim::getInstance()->request->getRootUri();
  $nb_items=count($items);
  if($items==NULL || count($items)==0)
      $lignes="Aucun items trouvé";
  else {
    $lignes='';
    foreach ($items as $e) {
      $rootReservationItem=Slim::getInstance()->urlFor('reservation-item', ['id_item' => $e->id_item]);
      $actions="<a href='$rootReservationItem' class='button'>Reserver</a>";
      $div_item=VueItem::afficherItem($e);
      $lignes.= <<<END
      <div class='div_ligne_liste'><div class='div_contenu'>$div_item</div><div class='div_action'>$actions</div></div>
END;
      if($items[sizeof($items)-1]!=$e)$lignes.="<hr />";
    }
  }

    return <<<END
    <div id='div_entete'>
      <h1>$categorie->nom</h1><br />
      <p>
        <i>$categorie->description</i><br /><br />
        $nb_items item(s)
      </p>    </div>
    <div id='div_liste'>
      $lignes
    </div>
END;
 }
}
