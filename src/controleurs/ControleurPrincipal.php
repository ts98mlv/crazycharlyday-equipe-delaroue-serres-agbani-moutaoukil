<?php

namespace garage\controleurs;

use garage\vues\VuePrincipale;
use garage\vues\VueItem;
use garage\modeles\Item;
use garage\modeles\Reservation;
use garage\modeles\Utilisateur;


/**
*  Classe qui répertorie les fonctions principales et globales du site
*/
class ControleurPrincipal{

  /**
  * Affiche la page d'accueil du site
  */
  public function afficherAccueil(){
      $v=new VuePrincipale(VuePrincipale::ACCUEIL);
      $v->render();
  }

  /**
  * Affiche la page de connexion/inscription
  */
  public function afficherConnexion(){
    $v=new VuePrincipale(VuePrincipale::CONNEXION_INSCRIPTION);
    $v->render();
  }

  /**
  * Affiche la page de profil utilisateur (grace a la barre de recherche sur la page d'accueil)
  */
  public function afficherUtilisateur($id_user=null){
    try{
      if($id_user!=null){
        //On vérifie id_liste
        if(!filter_var($id_user, FILTER_VALIDATE_INT))
          throw new \Exception("id_user incorect");

        $user = Utilisateur::where('id_user', '=', $id_user)->first();
      }
      else{
        if(!filter_var($_POST['recherche_email_personne'], FILTER_VALIDATE_EMAIL))
          throw new \Exception("email rentré invalide");

        $user = Utilisateur::where('email', '=', $_POST['recherche_email_personne'])->first();
      }

      if(!isset($user) || $user==null)
        throw new \Exception("utilisateur inconnu");

      $listes=Liste::where('id_user', '=', $user->id_user)->where('token', '!=', null)->get();

      $v=new VuePrincipale(VuePrincipale::UTILISATEUR, ['user' =>$user, 'listes' => $listes]);
      $v->render();
    }
    catch (\Exception $e){
      $v = new VuePrincipale(VuePrincipale::ERREUR, ['message' => "Impossible d'acceder à l'utilisateur: ".$e->getMessage()]);
      $v->render();
    }
  }

  /**
  * Affiche la page des participations de l'utilisateur connécté : tous les items qu'il a réservé
  */
  public function afficherParticipations(){
    try{
      //on vérifie que l'utilisateur est connecté
      if (!isset($_SESSION['user']))
        throw new \Exception("vous n'êtes pas connecté");

      $items=Item::select('item.nom', 'descr', 'tarif', 'img', 'id_liste')->join('reservation', 'reservation.id_item', '=', 'item.id_item')->where('id_user', '=', $_SESSION['user']['id_user'])->get();

      $v=new VuePrincipale(VuePrincipale::PARTICIPATIONS, ['items' => $items]);
      $v->render();
    }
    catch (\Exception $e){
      $v = new VuePrincipale(VuePrincipale::ERREUR, ['message' => "Impossible d'afficher les participations: ".$e->getMessage()]);
      $v->render();
    }
  }

  /**
  * Affiche la page de parametrage de ces informations personnelles
  */
  public function afficherInfosPersos_Param(){
      $v=new VuePrincipale(VuePrincipale::PARAMETRES);
      $v->render();
  }

  /**
  * Affiche la page de confirmation de supression du compte
  */
  public function confirmerSuppressionCompte(){
    $v=new VuePrincipale(VuePrincipale::SUPPRESSION_COMPTE);
    $v->render();
  }

  /**
  * Affiche la page d'aide
  */
  public function afficherAideinfo(){
      $v=new VuePrincipale(VuePrincipale::AIDE);
      $v->render();
  }

  /**
  * Affiche de choix de categorie
  */
  public function afficherChoixCategorie(){
      $v=new VuePrincipale(VuePrincipale::CHOIX_CATEGORIE);
      $v->render();
  }

  /**
  *affiche les reservations de l'utilisateur
  */
  public function afficherReservations(){
    $reservations = Reservation::where('id_user','=', $_SESSION['user']['id_user'])->get();

    $v = new VueItem(VueItem::MESRESERVATIONS, ['reservations' => $reservations]);
    $v->render();
  }
}
