<?php

namespace garage\controleurs;

use garage\modeles\Item;
use garage\modeles\Categorie;
use garage\modeles\Reservation;
use garage\modeles\Planning;
use garage\modeles\Creneau;
use garage\modeles\Utilisateur;
use garage\vues\VuePrincipale;
use garage\vues\VueCategorie;
use garage\vues\VueItem;
use Slim\Slim;

/**
* Classe qui regroupe les différentes fonctions relatives aux items
*/
class ControleurItem {

    /**
    * Affiche la page pour réserver un item
    *   @param int id de l'item à réserver
    */
    public function afficherCreationReservation($id_item){
      //try{
        //On verifie id_item
        if(!filter_var($id_item, FILTER_VALIDATE_INT))
          throw new \Exception("id_item n'est pas bon");

        //On séléctionne l'item
        $item=Item::where('id_item', '=', $id_item)->first();

        //On vérifie que l'item existe
        if(!isset($item))
          throw new \Exception("cet item n'existe pas");

        $item=Item::where('id_item', '=', $id_item)->first();
        $reservations = Reservation::where('id_item', '=', $id_item)->get();

        $v=new VueItem(VueItem::RESERVATION, ['item' => $item, 'reservations' => $reservations]);
        $v->render();
      /*}
      catch (\Exception $e){
        $v = new VuePrincipale(VuePrincipale::ERREUR, ['message' => "Impossible d'accéder à la creation d'un item: ".$e->getMessage()]);
        $v->render();
      }*/
    }

    public function confirmerReservation($id_item){
      //On verifie id_item
      if(!filter_var($id_item, FILTER_VALIDATE_INT))
        throw new \Exception("id_item n'est pas bon");

      //On séléctionne l'item à réserver
      $item=Item::where('id_item', '=', $id_item)->first();

      //On vérifie que l'item existe
      if(!isset($item))
        throw new \Exception("cet item n'existe pas");

      //On vérifie que l'utilisateur est connecté
      if (!isset($_SESSION['user']))
        throw new \Exception("vous n'êtes pas connecté");

      $reservation = Reservation::where('id_item', '=', $item->id_item);

      //On vérifie que l'item existe
      if(!isset($reservation))
        throw new \Exception("cet reservation n'existe pas");

      if($_SESSION['user']['level'] != 0)
        throw new \Exception("vous n'etes pas administrateur");

      $reservation->etat = "acceptee";
      $reservation->save();
    }

    /**
    * Reserve l'item en créant une instance de Reservation dans la base de donnée
    *   @param int id de l'item à réserver
    */
    public function reserverItem($id_item, $nb_jour, $nb_creneau){
      try{
        //On verifie id_item
        if(!filter_var($id_item, FILTER_VALIDATE_INT))
          throw new \Exception("id_item n'est pas bon");

        //On séléctionne l'item à réserver
        $item=Item::where('id_item', '=', $id_item)->first();

        //On vérifie que l'item existe
        if(!isset($item))
          throw new \Exception("cet item n'existe pas");

        //On vérifie que l'utilisateur est connecté
        if (!isset($_SESSION['user']))
          throw new \Exception("vous n'êtes pas connecté");

        if(isset($_POST['tarif']))$tarif=$_POST['tarif'];
        else $tarif=0;

        //On créer la reservation
        $r = new Reservation();
        $r->id_item = $id_item;
        $r->etat="demandee";
        $r->nb_jour = $nb_jour;
        $r->nb_creneau = $nb_creneau;
        $r->id_user=$_SESSION['user']['id_user'];
        $r->save();

        //On fait une redirection pour afficher la categorie
        $v = new VuePrincipale(VuePrincipale::INFO, ['message' => "La réservation à bien été prise en compte"]);
        $v->render();
      }
      catch (\Exception $e){
        $v = new VuePrincipale(VuePrincipale::ERREUR, ['message' => "Impossible de réserver cet item: ".$e->getMessage()]);
        $v->render();
      }
    }

    /**
    * Créer un item depuis un formulaire dans la categorie indiquée en parametre
    *   @param int id de la categorie dans laquel l'item va apparaitre
    *   @param $item_fourni item fourni pour la modification de l'item
    */
    public function creerItem($id_categorie){
      try{

        //On vérifie que tout les données formulaire sont remplies
        if(!isset($_POST['nom']) || !isset($_POST['descr']) || !isset($_POST['prix']))
          throw new \Exception("identification manquante");

        //On vérifie id_categorie
        if (!filter_var($id_categorie, FILTER_VALIDATE_INT))
          throw new \Exception("probleme dans id_categorie");

        //On séléctionne la categorie dans laquelle l'item va apparaitre
        $categorie = Categorie::where('id_categorie', '=', $id_categorie)->first();

        //On vérifie que la categorie existe
        if(!isset($categorie))
          throw new \Exception("la categorie n'existe pas");

        //On vérifie que l'utilisateur est connecté
        if (!isset($_SESSION['user']))
          throw new \Exception("vous n'êtes pas connecté");

        if(!is_numeric($_POST['prix']) && !is_int($_POST['prix']))
          throw new \Exception("vous devez saisir un nombre pour le prix: ".$_POST['prix']);

        //Si l'image à été séléctionnée
        if($_FILES['image']['name']!=null){
          //On vérifie qu'il n'y a pas eu d'erreur lors de l'upload
          if ($_FILES['image']['error'] > 0)
            throw new \Exception("erreur lors de l'upload de l'image. Code d'erreur: ".$_FILES['image']['error']);

          //On vérifie l'extension de l'image
          $extensions_valides = array('jpg', 'jpeg', 'gif', 'png');
          $extension_upload = strtolower(substr(strrchr($_FILES['image']['name'], '.'), 1));
          if (!in_array($extension_upload, $extensions_valides))
            throw new \Exception("extension de l'image inconnue");

          //On créé un nom de fichier pour stocker l'image et on la stock
          $nom_img=bin2hex(random_bytes(8)).'.'.$extension_upload;
          $chemin_img='img/'.$nom_img;
          if(!move_uploaded_file($_FILES['image']['tmp_name'], $chemin_img))
            throw new \Exception("erreur lors du déplacement de l'image");
        }
        else $nom_img='no-image.png';

        //On filtre les données
        $nom=filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
        $descr=filter_var($_POST['descr'], FILTER_SANITIZE_STRING);
        $prix=$_POST['prix'];

        //On modifie l'item et on l'enregistre dans la bdd
        $item=new Item();
        $item->id_categorie=$id_categorie;
        $item->nom=$nom;
        $item->descr=$descr;
        $item->tarif=$prix;
        $item->img=$nom_img;
        $item->save();

        //On fait une redirection vers les categories
        $app=Slim::getInstance();
        $redirection=$app->urlFor('afficherCategorie', ['param'=>$id_categorie]);
        $app->redirect($redirection);
      }
      catch (\Exception $e){
        $v = new VuePrincipale(VuePrincipale::ERREUR, ['message' => "Impossible de créer un item: ".$e->getMessage()]);
        $v->render();
      }
    }

    /**
    * Créer un item depuis un formulaire dans la categorie indiquée en parametre
    *   @param int id de la categorie dans laquel l'item va apparaitre
    *   @param $item_fourni item fourni pour la modification de l'item
    */
    public function modifierItem($id_item){
      try{

        //On vérifie id_item
        if (!filter_var($id_item, FILTER_VALIDATE_INT))
          throw new \Exception("id_item invalide");

        //On selectionne l'item
        $item = Item::where('id_item', '=', $id_item)->first();

        //On vérifie qu'il existe
        if(!isset($item) || $item==null)
            throw new \Exception("l'item n'existe pas");

        //On vérifie que tout les données formulaire sont remplis
        if(!isset($_POST['nom']) || !isset($_POST['descr']) || !isset($_POST['prix']))
          throw new \Exception("identification manquante");

        //On séléctionne la categorie dans laquelle l'item va apparaitre
        $categorie = Categorie::where('id_categorie', '=', $item->id_categorie)->first();

        //On vérifie que l'utilisateur est connecté
        if (!isset($_SESSION['user']))
          throw new \Exception("vous n'êtes pas connecté");

        //Si l'image à été séléctionnée
        if($_FILES['image']['name']!=null){
          //On vérifie qu'il n'y a pas eu d'erreur lors de l'upload
          if ($_FILES['image']['error'] > 0)
            throw new \Exception("erreur lors de l'upload de l'image. Code d'erreur: ".$_FILES['image']['error']);

          //On vérifie l'extension de l'image
          $extensions_valides = array('jpg', 'jpeg', 'gif', 'png');
          $extension_upload = strtolower(substr(strrchr($_FILES['image']['name'], '.'), 1));
          if (!in_array($extension_upload, $extensions_valides))
            throw new \Exception("extension de l'image inconnue");

          //On créé un nom de fichier pour stocker l'image et on la stock
          $nom_img=bin2hex(random_bytes(8)).'.'.$extension_upload;
          $chemin_img='img/'.$nom_img;
          if(!move_uploaded_file($_FILES['image']['tmp_name'], $chemin_img))
            throw new \Exception("erreur lors du déplacement de l'image");
          $item->img=$nom_img;
        }

        if(!is_numeric($_POST['prix']) && !is_int($_POST['prix']))
          throw new \Exception("vous devez saisir un nombre pour le prix: ".$_POST['prix']);

        //On filtre les données
        $nom=filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
        $descr=filter_var($_POST['descr'], FILTER_SANITIZE_STRING);
        $prix=$_POST['prix'];

        //On modifie l'item et on l'enregistre dans la bdd
        $item->nom=$nom;
        $item->descr=$descr;
        $item->tarif=$prix;
        $item->save();

        //On fait une redirection vers les categories
        $app=Slim::getInstance();
        $redirection=$app->urlFor('afficherCategorie', ['param'=>$item->id_categorie]);
        $app->redirect($redirection);
      }
      catch (\Exception $e){
        $v = new VuePrincipale(VuePrincipale::ERREUR, ['message' => "Impossible de modifier un item: ".$e->getMessage()]);
        $v->render();
      }
    }

    /***
     * methode qui permet de modifier un item
     * @param $id_item id de item a modifier
     */
    public function afficherModificationItem($id_item){
      try{
        //On vérifie id_item
        if (!filter_var($id_item, FILTER_VALIDATE_INT))
          throw new \Exception("id_item n'est pas bon");

        //On selectionne l'item
        $item = Item::where('id_item', '=', $id_item)->first();

        //On vérifie s'il existe
        if(!isset($item) || $item==null)
          throw new \Exception("l'item n'existe pas");

        //On séléctionne la categorie dans laquelle l'item va apparaitre
        $categorie = Categorie::where('id_categorie', '=', $item->id_categorie)->first();

        //On vérifie que l'utilisateur est connecté
        if (!isset($_SESSION['user']))
          throw new \Exception("vous n'êtes pas connecté");

        $v = new VueItem(VueItem::MODIFICATION, ['item' => $item]);
        $v->render();
        }
        catch(\Exception $e){
          $v = new VuePrincipale(VuePrincipale::ERREUR, ['message' => "Impossible de modifier un item: $e->getMessage()"]);
        }

    }

    /**
    * Affiche la page pour créer un item
    *   @param int id de la categorie dans laquelle l'item apparaitra
    */
    public function afficherCreationItem(){
      try{
        if (!isset($_SESSION['user']))
          throw new \Exception("vous n'êtes pas connecté");

        $v=new VueItem(VueItem::CREATION, ['id_categorie' => $id_categorie]);
        $v->render();
      }
      catch (\Exception $e){
        $v = new VuePrincipale(VuePrincipale::ERREUR, ['message' => "Impossible de creer un item: ".$e->getMessage()]);
        $v->render();
      }
    }

    /**
    * Supprime un item de la base de donnée. Impossible si l'item est réservé
    *   @param int id de l'item à supprimer
    */
    public function supprimerItem($id_item){
      try{
        //On verifie id_item
        if(!filter_var($id_item, FILTER_VALIDATE_INT))
          throw new \Exception("id_item n'est pas bon");

        //On séléctionne l'item
        $item=Item::where('id_item', '=', $id_item)->first();

        //On vérifie que l'item existe
        if(!isset($item))
          throw new \Exception("cet item n'existe pas");

        //On séléctionne le propriétaire de la categorie
        $id_user=Categorie::where('id_categorie', '=', $item->id_categorie)->first()->id_user;

        //On vérifie que l'utilisateur est connécté
        if (!isset($_SESSION['user']))
          throw new \Exception("vous n'êtes pas connecte");

        $categorie = Categorie::where('id_categorie', '=', $item->id_categorie)->first();

        $reservation=Reservation::where('id_item', '=', $id_item)->first();

        if(isset($reservation) && $reservation!=NULL){
          if(strtotime($categorie->expiration) > time())
            throw new \Exception("il est réservé");
          else $reservation->delete();
        }

        //Enfin on enregiste l'id de la categorie pour la réafficher et on supprimer l'item
        $id_categorie=$item->id_categorie;
        if($item->img!='no-image.png')unlink("img/$item->img");
        $item->delete();

        //On fait une redirection pour afficher la categorie
        $app=Slim::getInstance();
        $redirection=$app->urlFor('afficherCategorie', ['param'=>$id_categorie]);
        $app->redirect($redirection);
      }
      catch (\Exception $e){
        $v = new VuePrincipale(VuePrincipale::ERREUR, ['message' => 'Impossible de supprimer un item: '.$e->getMessage()]);
        $v->render();
      }
    }

    public function afficherVoitures(){
      $categorie = Categorie::where('nom', '=', "Voitures")->first();
      $voitures = Item::where("id_categorie", '=', $categorie->id_categorie)->get();

      $v = new VueItem(VueItem::AFFICHER, ['items' => $voitures, 'categorie' => $categorie]);
      $v->render();
    }

    public function afficherAteliers(){
      $categorie = Categorie::where('nom', '=', "Ateliers")->first();
      $ateliers = Item::where("id_categorie", '=', $categorie->id_categorie)->get();

      $v = new VueItem(VueItem::AFFICHER, ['items' => $ateliers, 'categorie' => $categorie]);
      $v->render();
    }

    public static function ajoutCommentaire($item_id, $content) {
        if ($content == "") {

        }

        //$msg = new MessagePublic();
        $msg->user_id = $_SESSION['profile']['uid'];
        $msg->item_id = $item_id;
        $msg->content = filter_var($content, FILTER_SANITIZE_SPECIAL_CHARS);
        $msg->publication = date("Y-m-d H:i:s");
        $msg->save();
    }



}
