<?php

namespace garage\controleurs;

use garage\modeles\Item;
use garage\modeles\Categorie;
use garage\modeles\Reservation;
use garage\modeles\Utilisateur;
use garage\vues\VuePrincipale;
use garage\vues\VueAdministrateur;
use garage\vues\VueCategorie;
use garage\vues\VueItem;
use Slim\Slim;

/**
* Classe qui regroupe les différentes fonctions relatives a l'administration
*/
class ControleurAdministration {

/**
*methode qui affiche la page d'administration
*/
public function afficherPageAdmin(){
  $v = new VueAdministrateur(VueAdministrateur::AFFICHAGE);
  $v ->render();
}

/**
*methode qui permet l'affichage de la creation de compte depuis la page d'administration
*/
public function afficherCreerCompte(){
  $v = new VueAdministrateur(VueAdministrateur::CREERCOMPTE);
  $v->render();
}

/**
*methode qui permet de créer un compte depuis l'administration
*/
public function creerCompte(){
  $a = new Authentification();
  $a->createUser("admin");
}

/**
*methode qui permet l'affichage des comptes créés
*/
public function voirComptes(){
  try{$comptes = Utilisateur::get();

  if(!isset($comptes) || count($comptes) ==0){
    throw new \Exception("pas de comptes existants");
  }

  $v = new VueAdministrateur(VueAdministrateur::AFFICHERCOMPTES, ['comptes'=>$comptes]);
  $v->render();
}
catch(\Exception $e){
  $v = new VuePrincipale(VuePrincipale::ERREUR, ['message'=>$e->getMessage()]);
  $v -> render();
}
}

/**
*methode qui permet l'affichage de la modification du compte*/
public function afficherModifierCompte($id){
  try{
    $compte = Utilisateur::where("id_user", "=", $id)->first();

    if(!isset($compte))
      throw new \Exception("pas de compte");

    $v = new VueAdministrateur(VueAdministrateur::MODIFIERCOMPTE, ['compte'=>$compte, 'id'=>$id]);
    $v->render();
  }
  catch(\Exception $e){
   $v = new VuePrincipale(VuePrincipale::ERREUR, ['message'=>$e->getMessage()]);
    $v->render();
  }
}

/**
*methode qui modifie le compte
*/
public function modifierCompte($id){
  try{
    $compte = Utilisateur::where("id_user", "=", $id)->first();

    if(!isset($compte))
      throw new \Exception("pas de compte");

    $compte->nom = $_POST['nom'];
    $compte->prenom = $_POST['prenom'];
    $compte->email = $_POST['email'];

    if($_POST['level'] === "compte administrateur")
      $compte->level = 0;
    else {
      $compte->level = 1;
    }
    $compte->save();

    $app =Slim::getInstance();
    $app->redirect($app->urlFor("administrer"));

  }catch(\Exception $e){
    $v = new VuePrincipale(VuePrincipale::ERREUR, ['message'=>$e->getMessage()]);
    $v->render();
  }
}

/**
*methode qui permet d'afficher les reservations
*/
public function afficherReservations(){
  try{
    $listes = Reservation::join("item", "item.id_item", "reservation.id_item")->get();

    if(!isset($listes))
      throw new \Exception("pas de reservations");

    $v = new VueAdministrateur(VueAdministrateur::AFFICHERLISTESRESERVEES, ['listes'=>$listes]);
    $v->render();
  }
  catch(\Exception $e){
    $v = new VuePrincipale(VuePrincipale::ERREUR, ['message'=>$e->getMessage()]);
    $v->render();
  }
}
}
