<?php

namespace garage\controleurs;

use garage\modeles\Utilisateur;
use garage\vues\VueParametres;
use garage\modeles\Liste;
use garage\modeles\Item;
use garage\vues\VuePrincipale;
use garage\modeles\Reservation;
use garage\controleurs\ControleurListe;
use Slim\Slim as Slim;

/**
* Classe qui gère l'authentification, la suppresion d'un compte, l'ajout d'un compte ...
*/
class Authentification{
  const UTILISATEUR=1;
  const ADMINISTRATEUR=2;

  /**
  * Authentifie un utilisateur déjà inscrit. Verifie le mot de passe. Charge le profil si l'utilisateur est bien authentifié
  */
  public function authenticate(){
    try{
      //verification des donnees transmises
      if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        throw new \Exception("ce n'est pas une adresse email");

      if(!filter_var($_POST['mdp'], FILTER_VALIDATE_REGEXP, array( "options"=> array( "regexp" => "/.{6,25}/"))))
        throw new \Exception("le mot de passe doit contenir entre 6 et 25 caracteres");

      //charger les donnees
      $email=$_POST['email'];
      $mdp=$_POST['mdp'];

      //charger mdp utilisateur
      $user_data=Utilisateur::where('email', '=', $email)->first();
      if(!isset($user_data) || $user_data==NULL)
        throw new \Exception("utilisateur inconnu");

      // vérifier $user->hash == hash($password)
      if (!password_verify($mdp, $user_data['mdp']))
        throw new \Exception("mot de passe incorrect");

      //chargement profil
      $this->loadProfile($user_data['id_user']);

      //redirection pour afficher l'accueil
      $app=Slim::getInstance();
      $redirection=$app->urlFor('accueil');
      $app->redirect($redirection);
    }
    catch(\Exception $e){
      $v=new VuePrincipale(VuePrincipale::ERREUR, ['message' => "Impossible de s'authentifier: ".$e->getMessage(), 'lien' =>"connexion-inscription"]);
      $v->render();
    }
  }

  /**
  * Charge le profil de ĺ'utilisateur en variable de session.
  *   @param $id_user identifiant de l'utilisateur a charger
  */
  public function loadProfile($id_user){
    // charger l'utilisateur et ses droits
    $user_data=Utilisateur::where('id_user', '=', $id_user)->first();

    //vérifier si l'utilisateur existe
    if(!isset($user_data) || $user_data==null)
      throw new \Exception("utilisateur inconnu");

    // détruire la variable de session
    if(isset($_SESSION['user']))unset($_SESSION['user']);

    // créer variable de session = profil charg
    $_SESSION['user']=$user_data;
  }

  /**
  * Creer un utilisateur grace au formulaire. Verifie toutes les données puis les inserts dans la base de données
  */
  public function createUser($parametre=null){
    try{
      //Verification des donnees transmises
      if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        throw new \Exception("Email incorrecte");

      if($_POST['mdp']!=$_POST['mdp_confirm'])
        throw new \Exception("vos mots de passe doivent correspondre");

      //On ajoute les données
      $prenom = filter_var($_POST['prenom'], FILTER_SANITIZE_STRING);
      $nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
      $email = $_POST['email'];
      $mdp=$_POST['mdp'];



      $test_user=Utilisateur::where('email', '=', $email)->first();
      if($test_user!=null)
        throw new \Exception("vous avez déjà une compte sur garage");

      // vérifier la conformité de $password avec la police
      if(!filter_var($mdp, FILTER_VALIDATE_REGEXP, array( "options"=> array( "regexp" => "/.{6,25}/"))))
        throw new \Exception("le mot de passe doit contenir entre 6 et 25 caracteres");

      //On hash le mot de passe
      $hash=password_hash($mdp, PASSWORD_DEFAULT);

      // créer et enregistrer l'utilisateur
      $new_user=new Utilisateur();
      $new_user->level=Authentification::UTILISATEUR;
      $new_user->mdp=$hash;
      $new_user->email=$email;
      $new_user->prenom=$prenom;
      $new_user->nom=$nom;

      if($parametre !== null){
        $level = $_POST['level'];
        if($level === "compte adminstrateur")
          $lvl = 0;
        else {
            $lvl = 1;
        }
        $new_user->level = $lvl;
      }
      $new_user->save();

      if($parametre !== null){
        $app = Slim::getInstance();
        $app->redirect($app->urlFor("administrer"));
      }
      else{
      $v=new VuePrincipale(VuePrincipale::INFO, ['message'=>"Compte crée avec succès. Connectez-vous pour découvrir les fonctionnalités", 'lien'=>"connexion-inscription"]);
      $v->render();
    }
    }
    catch(\Exception $e){
      $v=new VuePrincipale(VuePrincipale::ERREUR, ['message'=>"Impossible de créer un compte: ".$e->getMessage(), 'lien'=>"connexion-inscription"]);
      $v->render();
    }
  }

  /**
  * Déconnecte l'utilisateur actuellement connecté
  */
  public function logout(){
    // détruire la variable de session
    if(isset($_SESSION['user']))unset($_SESSION['user']);

    //On fait une redirection pour afficher l'accueil'
    $app=Slim::getInstance();
    $redirection=$app->urlFor('accueil');
    $app->redirect($redirection);
  }

  /**
  * Modifie les parametres d'un utilisateur
  */
  public function modifierParametres(){
      try{
        //Verification de $_SESSION
        if(!isset($_SESSION['user']['id_user']))
          throw new \Exception("vous n'êtes pas connécté");

        //On recupere l'utilisateur
        $user=Utilisateur::where('id_user', '=', $_SESSION['user']['id_user'])->first();

        //Verification des donnees transmises
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
          throw new \Exception("email incorrecte");

        //On ajoute les données
        $user->prenom = filter_var($_POST['prenom'], FILTER_SANITIZE_STRING);
        $user->nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
        $user->email = $_POST['email'];

        $test_user=Utilisateur::where('email', '=', $user->email)->first();
        if($test_user!=null && $user->email!=$_SESSION['user']['email'])
          throw new \Exception("cet email est déjà attribuée");

        if(isset($_POST['mdp']) && isset($_POST['mdp_confirm']) && $_POST['mdp']!=null){
          if($_POST['mdp'] != $_POST['mdp_confirm'])
            throw new \Exception("les mots de passe doivent correspondre");

          // vérifier la conformité de $password avec la police
          if(!filter_var($_POST['mdp'], FILTER_VALIDATE_REGEXP, array( "options"=> array( "regexp" => "/.{6,25}/"))))
            throw new \Exception("le mot de passe doit contenir entre 6 et 25 caracteres");

          //On hash le mot de passe
          $hash=password_hash($_POST['mdp'], PASSWORD_DEFAULT);
          $user->mdp=$hash;
        }

      $user->save();
      $this->loadProfile($user->id_user);

      //On fait une redirection pour afficher l'accueil'
      $app=Slim::getInstance();
      $redirection=$app->urlFor('accueil');
      $app->redirect($redirection);
      }
      catch(\Exception $e){
        $v=new VuePrincipale(VuePrincipale::ERREUR, ['message'=>"Impossible d'effectuer les changements: ".$e->getMessage()]);
        $v->render();
      }
  }

/**
* méthode qui premet la suppresion d'un compte dans la base de donnée
*/
  public function supprimerCompte(){
    try{
      //Verification de $_SESSION
      if(!isset($_SESSION['user']))
        throw new \Exception("vous n'êtes pas connécté");

      //On recupere l'utilisateur
      $user=Utilisateur::where('id_user', '=', $_SESSION['user']['id_user'])->first();

      //On récupere les listes
      $utilisateur = Utilisateur::where('id_user', '=', $_SESSION['user']['id_user'])->get();

    //  $c=new ControleurListe();

//       $reservations=Reservation::where('id_user', '=', $user->id_user)->delete();
//       foreach($listes as $liste){
//         $items=Item::where('id_liste', '=', $liste->id_liste)->delete();
//         $liste->delete();
//       }

      $user->delete();

      $this->logout();
      //On fait une redirection pour afficher l'accueil'
      $app=Slim::getInstance();
      $redirection=$app->urlFor('accueil');
      $app->redirect($redirection);
    }
    catch(\Exception $e){
      $v=new VuePrincipale(VuePrincipale::ERREUR, ['message'=>"Impossible de supprimer l'utisateur: ".$e->getMessage()]);
      $v->render();
    }
  }
}
